rws
===
random wallpaper select+sync

- Given a directory, chooses a random **file**[0] and sets as wallpaper.
- Given a file, sets the file as the wallpaper.

Syncs wal theme from wallpaper to Xresources.

License is MIT.

[0]: Meaning you should only have image files in the directory
