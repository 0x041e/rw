.TH RWS 1 "26 April 23"
.SH NAME
rws \- random wallpaper select+sync
.SH SYNOPSIS
\fBrws\fP [ -d \fIdirectory\fP | -f \fIfile\fP]

.SH DESCRIPTION
\fBrws\fP Syncs Xresources with the wal generated color scheme of the currently selected wallpaper.

.SS Options
.TP
\fB-d \fIdirectory\fR
Selects a random wallpaper from \fIdirectory\fP.
.TP
\fB-f \fIfile\fP
Sets \fIfile\fP as the wallpaper.
.SH "SEE ALSO"
wal: \fI<https://github.com/dylanaraps/pywal>\fP
feh: feh(1)
.SH BUGS
Please report bugs to \fI<https://gitlab.com/u2on/rws/-/issues>\fP

